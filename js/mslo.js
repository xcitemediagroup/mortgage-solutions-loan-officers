jQuery(document).ready(function( $ ) {	 
	 $("#search-by-state-zip").on('submit', function(e) {
		e.preventDefault();
		$(".loading").show();
		var fdc = new FormData();
		 $('#search-by-state-zip *').filter(':input').each(function(){
			fdc.append(this.name, this.value);
		});	 
		fdc.append('action', 'searchByStateZipContent');
		
		var fd = new FormData();
		 $('#search-by-state-zip *').filter(':input').each(function(){
			fd.append(this.name, this.value);
		});	 
		fd.append('action', 'searchByStateZip');
		
		jQuery.ajax({
			 'type' : "POST",
		   'url': my_ajax_object.ajax_url,
		   'data': fdc,
		   processData: false,
			contentType:false,
		   'success':function(res){
				 $('#mslo-search-results').html(res);
				 $(".loading").hide();
			 }
		  })
		
		jQuery.ajax({
		   'type' : "POST",
		   'url': my_ajax_object.ajax_url,
		   'data': fd,
		   processData: false,
			contentType:false,
		   'success':function(res){
			   $('#mslo-map').html(res);
			 $('#mslo-map').each(function(){
				render_map( $(this) );			 
			 });
		   }
	   });
    
    });
	
	 $("#search-by-loan-officer").on('submit', function(e) {
		e.preventDefault();
		$(".loading").show();
		var fdc = new FormData();
		 $('#search-by-loan-officer *').filter(':input').each(function(){
			fdc.append(this.name, this.value);
		});	 
		fdc.append('action', 'searchByLoNameContent');
		
		var fd = new FormData();
		 $('#search-by-loan-officer *').filter(':input').each(function(){
			fd.append(this.name, this.value);
		});	 
		fd.append('action', 'searchByLoName');
		
		jQuery.ajax({
			 'type' : "POST",
		   'url': my_ajax_object.ajax_url,
		   'data': fdc,
		   processData: false,
			contentType:false,
		   'success':function(res){
				 $('#mslo-search-results').html(res);
				 $(".loading").hide();
			 }
		  })
		
		jQuery.ajax({
		   'type' : "POST",
		   'url': my_ajax_object.ajax_url,
		   'data': fd,
		   processData: false,
			contentType:false,
		   'success':function(res){
			   $('#mslo-map').html(res);
			 $('#mslo-map').each(function(){
				render_map( $(this) );			 
			 });
		   }
	   });
    
    });
});