<?php
/**
 * Plugin Name: Mortgage Solutions - Loan Officers
 * Plugin URI: http://www.letsdowpthings.com
 * Description: Load Officer Functionality v01
 * Author: Melissa Dolly
 * Author URI: http://www.letsdowpthings.com
 * Version: 0.5
 * License: GPLv2 or later
 * Text Domain: ms-loan-officers
 */
 
 /**
 * Exit if accessed directly
 **/
 if ( ! defined( 'ABSPATH' ) ) { 
    exit; 
}

include( plugin_dir_path( __FILE__ ) . 'shortcodes.php');

	if (!class_exists('MS_LoanOfficers')) {
		class MS_LoanOfficers {
			public function __construct() {
				add_action( 'init', array($this,'create_mortgage_solutions_posttypes'));
				add_action('init', array($this, 'init'));	
			}

			/**
		 * to add the necessary actions for the plugin
		 */
		public function init() {
			add_action( 'add_meta_boxes', array($this,'add_loan_officer_meta_boxes'));
			add_action( 'edit_form_after_title', array($this,'add_before_editor'));
			add_action( 'save_post', array($this,'update_loan_officer_post'));
			add_filter( 'manage_loan_officer_posts_columns', array($this,'loan_officer_columns') );
			add_action( 'manage_loan_officer_posts_custom_column' , array($this,'loan_officer_column'), 10, 2 );
			add_filter( 'manage_location_posts_columns', array($this,'location_columns') );
			add_action( 'manage_location_posts_custom_column' , array($this,'location_column'), 10, 2 );
		}
			function add_before_editor($post) {
			  global $post;
			  if ($post->post_type == 'loan_officer') {
				  do_meta_boxes('loan_officer', 'pre_editor', $post);
			  }
			  if ($post->post_type == 'location') {
				  do_meta_boxes('location', 'pre_editor', $post);
			  }
			}
		
			function create_mortgage_solutions_posttypes() {
			  register_post_type( 'loan_officer',
				array(
				  'labels' => array(
					'name' => __( 'Loan Officers', 'ms-loan-officers' ),
					'singular_name' => __( 'Loan Officer', 'ms-loan-officers' ),
					'menu_name'          => __( 'Loan Officers', 'ms-loan-officers'),
				  ),
				  'public' => true,
				  'has_archive' => true,
                  'menu_icon' => 'dashicons-businessman',
				  'supports'  => array( 'title', 'editor', 'thumbnail')
				)
			  );
			  
			  register_post_type( 'location',
				array(
				  'labels' => array(
					'name' => __( 'Locations', 'ms-loan-officers' ),
					'singular_name' => __( 'Location', 'ms-loan-officers' ),
					'menu_name'          => __( 'Locations', 'ms-loan-officers'),
				  ),
				  'public' => true,
				  'has_archive' => true,
                  'menu_icon' => 'dashicons-location',
				  'supports'  => array( 'title','editor','thumbnail')
				)
			  );
		}
		
		function loan_officer_columns($columns) {
			unset(
				$columns['date']
			);
			$new_columns = array(
				'lo_first_name' => __('First Name', 'ms-loan-officers'),
				'lo_last_name' => __('Last Name', 'ms-loan-officers'),
				'lo_city' => __('City', 'ms-loan-officers'),
				'lo_state' => __('State', 'ms-loan-officers'),
				'lo_nmls' => __('NMLS', 'ms-loan-officers'),
				'lo_mlo' => __('MLO', 'ms-loan-officers'),
			);
			return array_merge($columns, $new_columns);
		}

		function loan_officer_column( $column, $post_id ) {
			switch ( $column ) {
				case 'title' :
					echo '<a href="/wp-admin/post.php?post='.$post_id.'&action=edit">'.get_post_meta( $post_id , 'lo_first_name' , true ).' '.get_post_meta( $post_id , 'lo_last_name' , true ).'</a>'; 
					break;
				case 'lo_first_name' :
					echo get_post_meta( $post_id , 'lo_first_name' , true ); 
					break;
				case 'lo_last_name' :
					echo get_post_meta( $post_id , 'lo_last_name' , true ); 
					break;
				case 'lo_city' :
					echo get_post_meta( $post_id , 'lo_city' , true ); 
					break;
				case 'lo_state' :
					echo get_post_meta( $post_id , 'lo_state' , true ); 
					break;
				case 'lo_nmls' :
					echo get_post_meta( $post_id , 'lo_nmls' , true ); 
					break;
				case 'lo_mlo' :
					echo get_post_meta( $post_id , 'lo_mlo' , true ); 
					break;

			}
		}
		
		function location_columns($columns) {
			unset(
				$columns['date']
			);
			$new_columns = array(
				'lo_address_one' => __('Address Line 1', 'ms-loan-officers'),
				'lo_address_two' => __('Address Line 2', 'ms-loan-officers'),
				'lo_city' => __('City', 'ms-loan-officers'),
				'lo_state' => __('State', 'ms-loan-officers'),
				'lo_zip' => __('Zip', 'ms-loan-officers'),
				'lo_phone' => __('Phone', 'ms-loan-officers'),
				'lo_fax' => __('Fax', 'ms-loan-officers'),
			);
			return array_merge($columns, $new_columns);
		}

		function location_column( $column, $post_id ) {
			switch ( $column ) {
				case 'lo_address_one' :
					echo get_post_meta( $post_id , 'lo_street_address' , true ); 
					break;
				case 'lo_address_two' :
					echo get_post_meta( $post_id , 'lo_street_address_two' , true ); 
					break;
				case 'lo_city' :
					echo get_post_meta( $post_id , 'lo_city' , true ); 
					break;
				case 'lo_state' :
					echo get_post_meta( $post_id , 'lo_state' , true ); 
					break;
				case 'lo_zip' :
					echo get_post_meta( $post_id , 'lo_zipcode' , true ); 
					break;
				case 'lo_phone' :
					echo get_post_meta( $post_id , 'lo_phone' , true ); 
					break;
				case 'lo_fax' :
					echo get_post_meta( $post_id , 'lo_fax' , true ); 
					break;

			}
		}

	

		
		/**
		 * Add WC Meta boxes.
		 */
		public function add_loan_officer_meta_boxes() {
            add_meta_box( 'loan-officer-name', sprintf( __( '%s', 'ms-loan-officers' ), 'Loan Officer Name' ), 'Loan_Officer_Meta_Box_Name_Information::output', 'loan_officer', 'pre_editor', 'default' );
            add_meta_box( 'loan-officer-licensing', sprintf( __( '%s', 'ms-loan-officers' ), 'Licensing Numbers' ), 'Loan_Officer_Meta_Box_Licensing_Information::output', 'loan_officer', 'pre_editor', 'default' );
             add_meta_box( 'loan-officer-address', sprintf( __( '%s', 'ms-loan-officers' ), 'Address' ), 'Loan_Officer_Meta_Box_Address_Information::output', 'loan_officer', 'pre_editor', 'default' );
            add_meta_box( 'loan-officer-contact', sprintf( __( '%s', 'ms-loan-officers' ), 'Contact Information' ), 'Loan_Officer_Meta_Box_Contact_Information::output', 'loan_officer', 'pre_editor', 'default' );
           add_meta_box( 'location-information', sprintf( __( '%s', 'ms-loan-officers' ), 'Location Information' ), 'Location_Meta_Box_Location_Information::output', 'location', 'pre_editor', 'default' );
            
            
            
		}
		
		public function update_loan_officer_post($post_id) {
			global $wpdb;
			if (!empty($_POST)) {
				
				if ( isset( $_POST['lo_first_name'] ) ) {
					update_post_meta($post_id, 'lo_first_name', $_POST['lo_first_name']);
				}
				if ( isset( $_POST['lo_last_name'] ) ) {
					update_post_meta($post_id, 'lo_last_name', $_POST['lo_last_name']);
				}
				if ( isset( $_POST['lo_first_name'] ) && isset( $_POST['lo_last_name'] )  ) {
					$where = array( 'ID' => $post_id );
					$lo_title = $_POST['lo_first_name'].' '.$_POST['lo_last_name'];
					$lo_name = strtolower($_POST['lo_first_name'].'-'.$_POST['lo_last_name']);
					$wpdb->update( $wpdb->posts, array( 'post_title' => $lo_title ), $where );
					$wpdb->update( $wpdb->posts, array( 'post_name' => $lo_name ), $where );
				}
				if ( isset( $_POST['lo_title'] ) ) {
					update_post_meta($post_id, 'lo_title', $_POST['lo_title']);
				}
				if ( isset( $_POST['lo_nmls'] ) ) {
					update_post_meta($post_id, 'lo_nmls', $_POST['lo_nmls']);
				}
				if ( isset( $_POST['lo_mlo'] ) ) {
					update_post_meta($post_id, 'lo_mlo', $_POST['lo_mlo']);
				}
				if ( isset( $_POST['lo_street_address'] ) ) {
					update_post_meta($post_id, 'lo_street_address', $_POST['lo_street_address']);
				}
				if ( isset( $_POST['lo_street_address_two'] ) ) {
					update_post_meta($post_id, 'lo_street_address_two', $_POST['lo_street_address_two']);
				}
				if ( isset( $_POST['lo_city'] ) ) {
					update_post_meta($post_id, 'lo_city', $_POST['lo_city']);
				}
				if ( isset( $_POST['lo_state'] ) ) {
					update_post_meta($post_id, 'lo_state', $_POST['lo_state']);
				}
				if ( isset( $_POST['lo_state'] ) ) {
					update_post_meta($post_id, 'lo_state', $_POST['lo_state']);
				}
				if ( isset( $_POST['lo_surefire'] ) ) {
					update_post_meta($post_id, 'lo_surefire', $_POST['lo_surefire']);
				}
				if ( isset( $_POST['lo_zipcode'] ) ) {
					update_post_meta($post_id, 'lo_zipcode', $_POST['lo_zipcode']);
					$address = get_post_meta(get_the_id(),'lo_street_address', true) . '+'  . '+' . get_post_meta(get_the_id(),'lo_city', true) . '+' . get_post_meta(get_the_id(),'lo_state', true) . '+' . get_post_meta(get_the_id(),'lo_zipcode', true) ;
					$address = str_replace(' ', '+', $address);
					$url = "https://maps.google.com/maps/api/geocode/json?address=".$address."&key=AIzaSyDUvWAnQKB6NfIoDt6alj35NIsC3dqjtS0";
					$response = file_get_contents($url);
					$response = json_decode($response, true);
					$lat = $response['results'][0]['geometry']['location']['lat'];
					$long = $response['results'][0]['geometry']['location']['lng'];
					update_post_meta($post_id, 'lo_latitude', $lat);
					update_post_meta($post_id, 'lo_longitude', $long);
					
				}
				if ( isset( $_POST['lo_phone'] ) ) {
					update_post_meta($post_id, 'lo_phone', $_POST['lo_phone']);
				}
				if ( isset( $_POST['lo_mobile'] ) ) {
					update_post_meta($post_id, 'lo_mobile', $_POST['lo_mobile']);
				}
				if ( isset( $_POST['lo_fax'] ) ) {
					update_post_meta($post_id, 'lo_fax', $_POST['lo_fax']);
				}
				if ( isset( $_POST['lo_email'] ) ) {
					update_post_meta($post_id, 'lo_email', $_POST['lo_email']);
				}
				if ( isset( $_POST['lo_hours'] ) ) {
					update_post_meta($post_id, 'lo_hours', $_POST['lo_hours']);
				}
                
			}
			 
			 
		}
			

		}
	}
	
				/**
 * WC_Meta_Box_Order_Data Class.
 */
class Loan_Officer_Meta_Box_Name_Information {

	/**
	 * Output the metabox.
	 *
	 * @param WP_Post $post
	 */
	public static function output( $post ) {
		global $post;
		if ( ! is_object( $post ) ) {
			$post = $post->ID;
		}
		
		
		$order = $post;
		?>
		<style type="text/css">
			#titlediv { display:none }
			.loan-officer-wrap input { width: 66% !important; float: right;}
            .loan-officer-wrap label { width: 30%; float: left; margin-right: 3%;}
	        .loan-officer-wrap p {    font-size: 13px;
    line-height: 2;
    float: left;
    width: 100%;
    margin: 0px 0px 12px 0px;}
            #pre_editor-sortables .postbox {width: 48%; float: left; margin-right: 2%;}
            #pre_editor-sortables .postbox:nth-child(even) {margin-right: 0px;}
            #pre_editor-sortables { width: 100%; display: inline-block;}
		</style>
		<div class="panel-wrap loan-officer-wrap">
			<input name="post_title" type="hidden" value="<?php echo empty( $post->post_title ) ?  : esc_attr( $post->post_title ); ?>" />
			<input name="post_status" type="hidden" value="<?php echo esc_attr( $post->post_status ); ?>" />
			<div id="loan_officer_data" class="panel">
				<div class="data_column_container">
					<div class="data_column">
						<p class="form-field form-field-wide">
                            <label for="lo_first-name">First Name:</label>
							<input name="lo_first_name" type="text" placeholder="First Name" value="<?php echo get_post_meta( $post->ID, 'lo_first_name', true ); ?>" />
						</p>
						<p class="form-field form-field-wide">
                            <label for="lo_last-name">Last Name:</label>
							<input name="lo_last_name" type="text" placeholder="Last Name" value="<?php echo get_post_meta( $post->ID, 'lo_last_name', true ); ?>" />
						</p>
						<p class="form-field form-field-wide">
                            <label for="lo_title">Title:</label>
							<input name="lo_title" type="text" placeholder="Title" value="<?php echo get_post_meta( $post->ID, 'lo_title', true ); ?>" />
						</p>
						<p class="form-field form-field-wide">
                            <label for="lo_surefire">Surefire data-user-id:</label>
							<input name="lo_surefire" type="text" placeholder="Surefire data-user-id" value="<?php echo get_post_meta( $post->ID, 'lo_surefire', true ); ?>" />
						</p>
					</div>
					</div>

					</div>
				</div>
				<div class="clear"></div>
		<?php

	}
	
	
}


class Loan_Officer_Meta_Box_Address_Information {

	/**
	 * Output the metabox.
	 *
	 * @param WP_Post $post
	 */
	public static function output( $post ) {
		global $post;
		if ( ! is_object( $post ) ) {
			$post = $post->ID;
		}
		
		
		?>
	
		<div class="panel-wrap loan-officer-wrap">
			
			<div id="loan_officer_address_data" class="panel">
				<div class="data_column_container">
					<div class="data_column">
						<p class="form-field form-field-wide">
                            <label for="lo_street_address">Street Address:</label>
							<input name="lo_street_address" type="text" placeholder="Street Address" value="<?php echo get_post_meta( $post->ID, 'lo_street_address', true ); ?>" />
						</p>
						<p class="form-field form-field-wide">
                            <label for="lo_city">City:</label>
							<input name="lo_city" type="text" placeholder="City" value="<?php echo get_post_meta( $post->ID, 'lo_city', true ); ?>" />
						</p>
						<p class="form-field form-field-wide">
                            <label for="lo_state">State:</label>
							<input name="lo_state" type="text" placeholder="State" value="<?php echo get_post_meta( $post->ID, 'lo_state', true ); ?>" />
						</p>
						<p class="form-field form-field-wide">
                            <label for="lo_zipcode">Zipcode:</label>
							<input name="lo_zipcode" type="text" placeholder="Zipcode" value="<?php echo get_post_meta( $post->ID, 'lo_zipcode', true ); ?>" />
						</p>
					</div>
				</div>
            </div>
        </div>
        <div class="clear"></div>
		<?php

	}
	
	
}

class Loan_Officer_Meta_Box_Contact_Information {

	/**
	 * Output the metabox.
	 *
	 * @param WP_Post $post
	 */
	public static function output( $post ) {
		global $post;
		if ( ! is_object( $post ) ) {
			$post = $post->ID;
		}
		
		
		?>
	
		<div class="panel-wrap loan-officer-wrap">
			
			<div id="loan_officer_address_data" class="panel">
				<div class="data_column_container">
					<div class="data_column">
						<p class="form-field form-field-wide">
                            <label for="lo_phone">Phone:</label>
							<input name="lo_phone" type="text" placeholder="Phone" value="<?php echo get_post_meta( $post->ID, 'lo_phone', true ); ?>" />
						</p>
						<p class="form-field form-field-wide">
                            <label for="lo_mobile">Mobile:</label>
							<input name="lo_mobile" type="text" placeholder="Mobile" value="<?php echo get_post_meta( $post->ID, 'lo_mobile', true ); ?>" />
						</p>
						<p class="form-field form-field-wide">
                            <label for="lo_fax">Fax:</label>
							<input name="lo_fax" type="text" placeholder="Fax" value="<?php echo get_post_meta( $post->ID, 'lo_fax', true ); ?>" />
						</p>
                        <p class="form-field form-field-wide">
                            <label for="lo_email">Email:</label>
							<input name="lo_email" type="email" placeholder="Email" value="<?php echo get_post_meta( $post->ID, 'lo_email', true ); ?>" />
						</p>
					</div>
				</div>
            </div>
        </div>
        <div class="clear"></div>
		<?php

	}
	
	
}

class Loan_Officer_Meta_Box_Licensing_Information {

	/**
	 * Output the metabox.
	 *
	 * @param WP_Post $post
	 */
	public static function output( $post ) {
		global $post;
		if ( ! is_object( $post ) ) {
			$post = $post->ID;
		}
		
		
		?>
	
		<div class="panel-wrap loan-officer-wrap">
			
			<div id="loan_officer_address_data" class="panel">
				<div class="data_column_container">
					<div class="data_column">
                        <p class="form-field form-field-wide">
                            <label for="lo_nmls">NMLS:</label>
							<input name="lo_nmls" type="text" placeholder="NMLS" value="<?php echo get_post_meta( $post->ID, 'lo_nmls', true ); ?>" />
						</p>
						<p class="form-field form-field-wide">
                            <label for="lo_mlo">MLO:</label>
							<input name="lo_mlo" type="text" placeholder="MLO" value="<?php echo get_post_meta( $post->ID, 'lo_mlo', true ); ?>" />
						</p>
					</div>
				</div>
            </div>
        </div>
        <div class="clear"></div>
		<?php

	}
	
	
}

class Location_Meta_Box_Location_Information {

	/**
	 * Output the metabox.
	 *
	 * @param WP_Post $post
	 */
	public static function output( $post ) {
		global $post;
		if ( ! is_object( $post ) ) {
			$post = $post->ID;
		}
		
		
		?>
		<style type="text/css">
			.location-wrap input { width: 66% !important; float: right;}
            .location-wrap label { width: 30%; float: left; margin-right: 3%;}
	        .location-wrap p { font-size: 13px;  line-height: 2;  float: left;  width: 100%;  margin: 0px 0px 12px 0px;}
            #pre_editor-sortables .postbox {width: 100%;  margin-right: 2%;}
            #pre_editor-sortables { width: 100%; display: inline-block; float: left; margin-top: 20px;}
		</style>
		<div class="panel-wrap location-wrap">
			
			<div id="location_information" class="panel">
				<div class="data_column_container">
					<div class="data_column">
						<div class="data_column">
						<p class="form-field form-field-wide">
                            <label for="lo_street_address">Address Line 1:</label>
							<input name="lo_street_address" type="text" placeholder="Address Line 1" value="<?php echo get_post_meta( $post->ID, 'lo_street_address', true ); ?>" />
						</p>
						<p class="form-field form-field-wide">
                            <label for="lo_street_address_two">Address Line 2:</label>
							<input name="lo_street_address_two" type="text" placeholder="Address Line 2" value="<?php echo get_post_meta( $post->ID, 'lo_street_address_two', true ); ?>" />
						</p>
						<p class="form-field form-field-wide">
                            <label for="lo_city">City:</label>
							<input name="lo_city" type="text" placeholder="City" value="<?php echo get_post_meta( $post->ID, 'lo_city', true ); ?>" />
						</p>
						<p class="form-field form-field-wide">
                            <label for="lo_state">State:</label>
							<input name="lo_state" type="text" placeholder="State" value="<?php echo get_post_meta( $post->ID, 'lo_state', true ); ?>" />
						</p>
						<p class="form-field form-field-wide">
                            <label for="lo_zipcode">Zipcode:</label>
							<input name="lo_zipcode" type="text" placeholder="Zipcode" value="<?php echo get_post_meta( $post->ID, 'lo_zipcode', true ); ?>" />
						</p>
						<p class="form-field form-field-wide">
                            <label for="lo_phone">Phone:</label>
							<input name="lo_phone" type="text" placeholder="Phone" value="<?php echo get_post_meta( $post->ID, 'lo_phone', true ); ?>" />
						</p>
						<p class="form-field form-field-wide">
                            <label for="lo_fax">Fax:</label>
							<input name="lo_fax" type="text" placeholder="Fax" value="<?php echo get_post_meta( $post->ID, 'lo_fax', true ); ?>" />
						</p>
						<p class="form-field form-field-wide">
                            <label for="lo_hours">Hours:</label>
							<textarea name="lo_hours" rows="4" cols="50"></textarea>
						</p>
						
					
					</div>
				</div>
            </div>
        </div>
        <div class="clear"></div>
		<?php

	}
	
	
}

	add_action('wp_ajax_searchByStateZip', 'searchByStateZip');
	add_action( 'wp_ajax_nopriv_searchByStateZip', 'searchByStateZip' );
	
	function searchByStateZip(){
		global $wpdb;
		$map_query = new WP_Query();
		$statesreversed = array(
	'Alabama'=>'AL',
	'Alaska'=>'AK',
	'Arizona'=>'AZ',
	'Arkansas'=>'AR',
	'California'=>'CA',
	'Colorado'=>'CO',
	'Connecticut'=>'CT',
	'Delaware'=>'DE',
	'Florida'=>'FL',
	'Georgia'=>'GA',
	'Hawaii'=>'HI',
	'Idaho'=>'ID',
	'Illinois'=>'IL',
	'Indiana'=>'IN',
	'Iowa'=>'IA',
	'Kansas'=>'KS',
	'Kentucky'=>'KY',
	'Louisiana'=>'LA',
	'Maine'=>'ME',
	'Maryland'=>'MD',
	'Massachusetts'=>'MA',
	'Michigan'=>'MI',
	'Minnesota'=>'MN',
	'Mississippi'=>'MS',
	'Missouri'=>'MO',
	'Montana'=>'MT',
	'Nebraska'=>'NE',
	'Nevada'=>'NV',
	'New Hampshire'=>'NH',
	'New Jersey'=>'NJ',
	'New Mexico'=>'NM',
	'New York'=>'NY',
	'North Carolina'=>'NC',
	'North Dakota'=>'ND',
	'Ohio'=>'OH',
	'Oklahoma'=>'OK',
	'Oregon'=>'OR',
	'Pennsylvania'=>'PA',
	'Rhode Island'=>'RI',
	'South Carolina'=>'SC',
	'South Dakota'=>'SD',
	'Tennessee'=>'TN',
	'Texas'=>'TX',
	'Utah'=>'UT',
	'Vermont'=>'VT',
	'Virginia'=>'VA',
	'Washington'=>'WA',
	'West Virginia'=>'WV',
	'Wisconsin'=>'WI',
	'Wyoming'=>'WY'
	);
	
	if ($_POST['select-zip'] != '') {
		$url = "https://maps.google.com/maps/api/geocode/json?address=".$_POST['select-zip']."&key=AIzaSyDUvWAnQKB6NfIoDt6alj35NIsC3dqjtS0";
					$response = file_get_contents($url);
					$response = json_decode($response, true);
					$fLat = $response['results'][0]['geometry']['location']['lat'];
					$fLon = $response['results'][0]['geometry']['location']['lng'];
					$locationsbyzip = $wpdb->get_results( "
					SELECT post_id, latitude, longitude, acos(sin(Latitude * 0.0175) * sin($fLat * 0.0175) 
               + cos(Latitude * 0.0175) * cos($fLat * 0.0175) *    
                 cos(($fLon * 0.0175) - (Longitude * 0.0175))
              ) * 3959 AS `distance`
				FROM (
               	 SELECT latitudes.`post_id`, latitude, longitude FROM 
 					( SELECT `post_id`, `meta_value` AS 'latitude' FROM `wp_postmeta` WHERE `meta_key` = 'lo_latitude' ) AS latitudes
					JOIN
					( SELECT `post_id`, `meta_value` AS 'longitude' FROM `wp_postmeta` WHERE `meta_key` = 'lo_longitude') AS longitudes
 					 ON latitudes.`post_id` = longitudes.`post_id`
                ) AS distanceposts
				WHERE
				acos(sin(Latitude * 0.0175) * sin($fLat * 0.0175) 
               + cos(Latitude * 0.0175) * cos($fLat * 0.0175) *    
                 cos(($fLon * 0.0175) - (Longitude * 0.0175))
              ) * 3959 <= 500
				ORDER BY `distance`",ARRAY_A
				);
				foreach ( $locationsbyzip as $locationbyzip ) {
					$args = array(
						'posts_per_page' => -1,
						'post_type' => 'location',
						'p'         => $locationbyzip['post_id'],
					);
				$map_query = new WP_Query($args);
		if ( $map_query->have_posts() ){
			while ( $map_query->have_posts() ) {
				$map_query->the_post();
					$lat = get_post_meta(get_the_id(),'lo_latitude', true);
					$long = get_post_meta(get_the_id(),'lo_longitude', true);
					 
						echo '<div class="marker" data-lat="'.$lat.'" data-lng="'.$long.'">';
						echo '<div class="map-right"><h5><a href="'.get_the_permalink().'">'.get_the_title().'</a></h5>';
						echo '</div>';
						echo '</div>';
			
			}
			
			wp_reset_postdata();
		}
				}
	} else if ($_POST['select-state'] != '') {	
		$smallstate = $statesreversed[$_POST['select-state']];
		$state = array(
            'key'     => 'lo_state',
			'value'   => $smallstate,
        );
		$args = array(
			'posts_per_page' => -1,
			'post_type' => 'location',
			'posts_per_page' => 1000,
			'meta_query' => array(
				$state,
			),
		);
		$map_query = new WP_Query($args);
		if ( $map_query->have_posts() ){
			while ( $map_query->have_posts() ) {
				$map_query->the_post();
					$lat = get_post_meta(get_the_id(),'lo_latitude', true);
					$long = get_post_meta(get_the_id(),'lo_longitude', true);
					 
						echo '<div class="marker" data-lat="'.$lat.'" data-lng="'.$long.'">';
						echo '<div class="map-right"><h5><a href="'.get_the_permalink().'">'.get_the_title().'</a></h5>';
						echo '</div>';
						echo '</div>';
			
			}
			
			wp_reset_postdata();
		}
	}
	
	

		
	
		wp_die();
	}
	
	add_action('wp_ajax_searchByStateZipContent', 'searchByStateZipContent');
	add_action( 'wp_ajax_nopriv_searchByStateZipContent', 'searchByStateZipContent' );
	
	function searchByStateZipContent(){
		global $wpdb;
		$content_query = new WP_Query();
		$statesreversed = array(
			'Alabama'=>'AL',
			'Alaska'=>'AK',
			'Arizona'=>'AZ',
			'Arkansas'=>'AR',
			'California'=>'CA',
			'Colorado'=>'CO',
			'Connecticut'=>'CT',
			'Delaware'=>'DE',
			'Florida'=>'FL',
			'Georgia'=>'GA',
			'Hawaii'=>'HI',
			'Idaho'=>'ID',
			'Illinois'=>'IL',
			'Indiana'=>'IN',
			'Iowa'=>'IA',
			'Kansas'=>'KS',
			'Kentucky'=>'KY',
			'Louisiana'=>'LA',
			'Maine'=>'ME',
			'Maryland'=>'MD',
			'Massachusetts'=>'MA',
			'Michigan'=>'MI',
			'Minnesota'=>'MN',
			'Mississippi'=>'MS',
			'Missouri'=>'MO',
			'Montana'=>'MT',
			'Nebraska'=>'NE',
			'Nevada'=>'NV',
			'New Hampshire'=>'NH',
			'New Jersey'=>'NJ',
			'New Mexico'=>'NM',
			'New York'=>'NY',
			'North Carolina'=>'NC',
			'North Dakota'=>'ND',
			'Ohio'=>'OH',
			'Oklahoma'=>'OK',
			'Oregon'=>'OR',
			'Pennsylvania'=>'PA',
			'Rhode Island'=>'RI',
			'South Carolina'=>'SC',
			'South Dakota'=>'SD',
			'Tennessee'=>'TN',
			'Texas'=>'TX',
			'Utah'=>'UT',
			'Vermont'=>'VT',
			'Virginia'=>'VA',
			'Washington'=>'WA',
			'West Virginia'=>'WV',
			'Wisconsin'=>'WI',
			'Wyoming'=>'WY'
			);
			
			
			
			if ($_POST['select-zip'] != '') {
				$url = "https://maps.google.com/maps/api/geocode/json?address=".$_POST['select-zip']."&key=AIzaSyDUvWAnQKB6NfIoDt6alj35NIsC3dqjtS0";
					$response = file_get_contents($url);
					$response = json_decode($response, true);
					$fLat = $response['results'][0]['geometry']['location']['lat'];
					$fLon = $response['results'][0]['geometry']['location']['lng'];
					$locationsbyzip = $wpdb->get_results( "
					SELECT post_id, latitude, longitude, acos(sin(Latitude * 0.0175) * sin($fLat * 0.0175) 
               + cos(Latitude * 0.0175) * cos($fLat * 0.0175) *    
                 cos(($fLon * 0.0175) - (Longitude * 0.0175))
              ) * 3959 AS `distance`
				FROM (
               	 SELECT latitudes.`post_id`, latitude, longitude FROM 
 					( SELECT `post_id`, `meta_value` AS 'latitude' FROM `wp_postmeta` WHERE `meta_key` = 'lo_latitude' ) AS latitudes
					JOIN
					( SELECT `post_id`, `meta_value` AS 'longitude' FROM `wp_postmeta` WHERE `meta_key` = 'lo_longitude') AS longitudes
 					 ON latitudes.`post_id` = longitudes.`post_id`
                ) AS distanceposts
				WHERE
				acos(sin(Latitude * 0.0175) * sin($fLat * 0.0175) 
               + cos(Latitude * 0.0175) * cos($fLat * 0.0175) *    
                 cos(($fLon * 0.0175) - (Longitude * 0.0175))
              ) * 3959 <= 500
				ORDER BY `distance`",ARRAY_A
				);
				foreach ( $locationsbyzip as $locationbyzip ) {
					$args = array(
						'posts_per_page' => -1,
						'post_type' => 'location',
						'p'         => $locationbyzip['post_id'],
					);
				$content_query = new WP_Query($args);
				if ( $content_query->have_posts() ){
					while ( $content_query->have_posts() ) {
						$content_query->the_post();
						$address = get_post_meta(get_the_id(),'lo_street_address', true) . '+'  . '+' . get_post_meta(get_the_id(),'lo_city', true) . '+' . get_post_meta(get_the_id(),'lo_state', true) . '+' . get_post_meta(get_the_id(),'lo_zipcode', true) ;
						$address = str_replace(' ', '+', $address);
						echo '<div class="mslo-search-result">';
						echo '<h4>'.get_the_title().'</h4>';
						echo '<p>'.get_post_meta(get_the_id(), 'lo_street_address', true ).'</p>';
						echo '<p>'.get_post_meta(get_the_id(), 'lo_street_address_two', true ).'</p>';
						echo '<p>'.get_post_meta(get_the_id(), 'lo_city', true ).', '.get_post_meta(get_the_id(), 'lo_state', true ).' '.get_post_meta(get_the_id(), 'lo_zipcode', true ).'</p>';
						echo '<p>'.get_post_meta(get_the_id(), 'lo_phone', true ).'</p>';
						echo '<p>'.get_post_meta(get_the_id(), 'lo_fax', true ).'</p>';
						echo '<div class="cta-bttn-div blue">
										<a target="_blank" class="cta-bttn blue" href="'.get_the_permalink().'">Website</a>
									</div>';
						echo '<div class="cta-bttn-div blue directions-button">
										<a target="_blank" class="cta-bttn blue" href="http://maps.google.com/maps?daddr='.$address.'">Directions</a>
									</div>';
						echo '</div>';
							
					}
						
						wp_reset_postdata();
					}
				}
			} else if ($_POST['select-state'] != '') {
				$smallstate = $statesreversed[$_POST['select-state']];
				$state = array(
					'key'     => 'lo_state',
					'value'   => $smallstate,
				);
				$args = array(
					'post_type' => 'location',
					'posts_per_page' => -1,
					'meta_query' => array(
						$state,
					),
				);
				$content_query = new WP_Query($args);
				if ( $content_query->have_posts() ){
					while ( $content_query->have_posts() ) {
						$content_query->the_post();
						$address = get_post_meta(get_the_id(),'lo_street_address', true) . '+'  . '+' . get_post_meta(get_the_id(),'lo_city', true) . '+' . get_post_meta(get_the_id(),'lo_state', true) . '+' . get_post_meta(get_the_id(),'lo_zipcode', true) ;
							$address = str_replace(' ', '+', $address);
							echo '<div class="mslo-search-result">';
							echo '<h4>'.get_the_title().'</h4>';
							echo '<p>'.get_post_meta(get_the_id(), 'lo_street_address', true ).'</p>';
							echo '<p>'.get_post_meta(get_the_id(), 'lo_street_address_two', true ).'</p>';
							echo '<p>'.get_post_meta(get_the_id(), 'lo_city', true ).', '.get_post_meta(get_the_id(), 'lo_state', true ).' '.get_post_meta(get_the_id(), 'lo_zipcode', true ).'</p>';
							echo '<p>'.get_post_meta(get_the_id(), 'lo_phone', true ).'</p>';
							echo '<p>'.get_post_meta(get_the_id(), 'lo_fax', true ).'</p>';
							echo '<div class="cta-bttn-div blue">
											<a target="_blank" class="cta-bttn blue" href="'.get_the_permalink().'">Website</a>
										</div>';
							echo '<div class="cta-bttn-div blue directions-button">
											<a target="_blank" class="cta-bttn blue" href="http://maps.google.com/maps?daddr='.$address.'">Directions</a>
										</div>';
							echo '</div>';
					}
					
					wp_reset_postdata();
				}
			}
		
		wp_die();
	}
	
	add_action('wp_ajax_searchByLoNameContent', 'searchByLoNameContent');
	add_action( 'wp_ajax_nopriv_searchByLoNameContent', 'searchByLoNameContent' );
	
	function searchByLoNameContent(){
		global $wpdb;
		$content_query = new WP_Query();
	
	if ($_POST['search-lo'] != '') {
		$first = array(
            'key'     => 'lo_first_name',
			'value'   => $_POST['search-lo'],
			'meta_compare' => 'LIKE'
        );
		$last = array(
            'key'     => 'lo_last_name',
			'value'   => $_POST['search-lo'],
			'meta_compare' => 'LIKE'
        );
	}
	
		
	$args = array(
			'post_type' => 'loan_officer',
			'posts_per_page' => -1,
			'meta_query' => array(
				$zip,
				$state,
			),
		);
		$content_query = new WP_Query($args);
		if ( $content_query->have_posts() ){
			while ( $content_query->have_posts() ) {
				$content_query->the_post();
					$address = get_post_meta(get_the_id(),'lo_street_address', true) . '+'  . '+' . get_post_meta(get_the_id(),'lo_city', true) . '+' . get_post_meta(get_the_id(),'lo_state', true) . '+' . get_post_meta(get_the_id(),'lo_zipcode', true) ;
					$address = str_replace(' ', '+', $address);
					echo '<div class="mslo-search-result">';
					echo '<div class="lo-image">'.get_the_post_thumbnail(get_the_id()).'</div>';
					echo '<div class="lo-content">';
					echo '<h4>'.get_the_title().'</h4>';
					echo '<p>'.get_post_meta(get_the_id(), 'lo_phone', true ).'</p>';
					echo '<p>'.get_post_meta(get_the_id(), 'lo_street_address', true ).'</p>';
					echo '<p>'.get_post_meta(get_the_id(), 'lo_street_address_two', true ).'</p>';
					echo '<p>'.get_post_meta(get_the_id(), 'lo_city', true ).', '.get_post_meta(get_the_id(), 'lo_state', true ).' '.get_post_meta(get_the_id(), 'lo_zipcode', true ).'</p>';
					echo '</div>';
					echo '<div class="clearfix"></div><div class="cta-bttn-div blue">
                                    <a target="_blank" class="cta-bttn blue" href="'.get_the_permalink().'">Website</a>
                                </div>';
					echo '<div class="cta-bttn-div blue directions-button">
                                    <a target="_blank" class="cta-bttn blue" href="/ConsumerPortal/Inquiry/GetStarted&LO='.get_the_id().'">Apply</a>
                                </div>';
					echo '</div>';
			
			}
			
			wp_reset_postdata();
		}
		wp_die();
	}
	
		add_action('wp_ajax_searchByLoName', 'searchByLoName');
		add_action('wp_ajax_nopriv_searchByLoName', 'searchByLoName');
	
	function searchByLoName(){
		global $wpdb;
		$content_query = new WP_Query();
	
	if ($_POST['search-lo'] != '') {
		$first = array(
            'key'     => 'lo_first_name',
			'value'   => $_POST['search-lo'],
			'meta_compare' => 'LIKE'
        );
		$last = array(
            'key'     => 'lo_last_name',
			'value'   => $_POST['search-lo'],
			'meta_compare' => 'LIKE'
        );
	}
	
		
	$args = array(
			'post_type' => 'loan_officer',
			'posts_per_page' => -1,
			'meta_query' => array(
				$zip,
				$state,
			),
		);
		$content_query = new WP_Query($args);
		if ( $content_query->have_posts() ){
			while ( $content_query->have_posts() ) {
				$content_query->the_post();
					$lat = get_post_meta(get_the_id(),'lo_latitude', true);
					$long = get_post_meta(get_the_id(),'lo_longitude', true);
					 
						echo '<div class="marker" data-lat="'.$lat.'" data-lng="'.$long.'">';
						echo '<div class="map-right"><h5><a href="'.get_the_permalink().'">'.get_the_title().'</a></h5>';
						echo '</div>';
						echo '</div>';
			
			}
			
			wp_reset_postdata();
		}
		wp_die();
	}
	
	function get_custom_post_type_template($single_template) {
     global $post;

     if ($post->post_type == 'loan_officer') {
          $single_template = dirname( __FILE__ ) . '/loan-officer-template.php';
     } else if ($post->post_type == 'location') {
		 $single_template = dirname( __FILE__ ) . '/location-template.php';
	 }
     return $single_template;
}
add_filter( 'single_template', 'get_custom_post_type_template' );
	
	
	/*
   * Instantiate plugin class and add it to the set of globals.
   */
  $ms_loanofficers = new MS_LoanOfficers();

  $plugin = plugin_basename( __FILE__ );



?>