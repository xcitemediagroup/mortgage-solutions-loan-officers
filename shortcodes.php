<?php
function mslo_branches_function() {		
	add_ms_scripts();
	global $wpdb;
	$states = new WP_Query( array( 'post_type' => 'location', 'posts_per_page' => -1));
	$us_states = array(
		'AL'=>'Alabama',
		'AK'=>'Alaska',
		'AZ'=>'Arizona',
		'AR'=>'Arkansas',
		'CA'=>'California',
		'CO'=>'Colorado',
		'CT'=>'Connecticut',
		'DE'=>'Delaware',
		'DC'=>'District of Columbia',
		'FL'=>'Florida',
		'GA'=>'Georgia',
		'HI'=>'Hawaii',
		'ID'=>'Idaho',
		'IL'=>'Illinois',
		'IN'=>'Indiana',
		'IA'=>'Iowa',
		'KS'=>'Kansas',
		'KY'=>'Kentucky',
		'LA'=>'Louisiana',
		'ME'=>'Maine',
		'MD'=>'Maryland',
		'MA'=>'Massachusetts',
		'MI'=>'Michigan',
		'MN'=>'Minnesota',
		'MS'=>'Mississippi',
		'MO'=>'Missouri',
		'MT'=>'Montana',
		'NE'=>'Nebraska',
		'NV'=>'Nevada',
		'NH'=>'New Hampshire',
		'NJ'=>'New Jersey',
		'NM'=>'New Mexico',
		'NY'=>'New York',
		'NC'=>'North Carolina',
		'ND'=>'North Dakota',
		'OH'=>'Ohio',
		'OK'=>'Oklahoma',
		'OR'=>'Oregon',
		'PA'=>'Pennsylvania',
		'RI'=>'Rhode Island',
		'SC'=>'South Carolina',
		'SD'=>'South Dakota',
		'TN'=>'Tennessee',
		'TX'=>'Texas',
		'UT'=>'Utah',
		'VT'=>'Vermont',
		'VA'=>'Virginia',
		'WA'=>'Washington',
		'WV'=>'West Virginia',
		'WI'=>'Wisconsin',
		'WY'=>'Wyoming',
	);
	ob_start();
?>

	<div id="mslo-branches-officers">
		<?php
			$available_states = array();
			if ( $states->have_posts() ) : while ( $states->have_posts() ) : $states->the_post();
				$thisstate = strtoupper(get_post_meta(get_the_id(),'lo_state', true));
				array_push($available_states, $thisstate);
			endwhile;
			wp_reset_postdata();
			endif;
			$available_states = array_unique($available_states);
			sort($available_states);
		?>
		<div id="mslo-branches">
			<h3>FIND A BRANCH</h3>
			<form id="search-by-state-zip">
				<select name="select-state">
					<option>Select a state</option>
					<?php
						foreach ($available_states as $available_state) {
							echo '<option>'.$us_states[$available_state].'</option>';
						}
					?>
				</select>
				<input type="text" name="select-zip" placeholder="Enter Zip">
				<button id="input_2" type="submit" class="form-submit-button" data-component="button">
				  FIND A BRANCH
				</button>
			</form>
		</div>
		
		<div id="mslo-officers">
			<h3>FIND A LOAN EXPERT</h3>
			<form id="search-by-loan-officer">
				<input name="search-lo" type="text" placeholder="Search by name">
				<button id="input_2" type="submit" class="form-submit-button" data-component="button">
				  FIND A LOAN OFFICER
				</button>
			</form>
		</div>
		
		<div id="mslo-search-results">
			
		</div>

	</div>
	
	<div id="mslo-map-container">
			<?php
				$map_query = new WP_Query();
				$map_query->query(array( 'post_type' => 'location','posts_per_page' => -1));
				if ( $map_query->have_posts() ){
					echo '<div id="mslo-map">';
					while ( $map_query->have_posts() ) {
						$map_query->the_post();
							$lat = get_post_meta(get_the_id(),'lo_latitude', true);
					$long = get_post_meta(get_the_id(),'lo_longitude', true);
							 
								echo '<div class="marker" data-lat="'.$lat.'" data-lng="'.$long.'">';
								echo '<div class="map-right"><h5><a href="'.get_the_permalink().'">'.get_the_title().'</a></h5>';
								echo '</div>';
								echo '</div>';
					
					}
					
					echo '</div>';
					/* Restore original Post Data */
					wp_reset_postdata();
				}
			?>
	</div>
			
<?php
	return ob_get_clean();		
}
			
add_shortcode( 'mslo_branches', 'mslo_branches_function' );
	
function add_ms_scripts() {
	
	wp_enqueue_style( 'ms-style', plugins_url('mortgage-solutions-loan-officers/css/mslo.css'));
	wp_enqueue_script( 'google-map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAtMm0BLdI6bgOzCThlNeNdWqDEOL8DXE8', array(), '3', true );
	wp_enqueue_script( 'ms-maps', plugins_url('mortgage-solutions-loan-officers/js/google-maps.js'));
	wp_enqueue_script( 'ms-script', plugins_url('mortgage-solutions-loan-officers/js/mslo.js'));
	wp_localize_script( 'ms-script', 'my_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );		
}