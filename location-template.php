<?php get_header(); ?>
<link rel="stylesheet" href="<?php echo plugins_url('mortgage-solutions-loan-officers/css/mslo.css');?>">
<div class="hero-box loan-officers" >
	<img style="opacity: 1;" src="https://msdev.shomptonlabs.com/wp-content/uploads/2018/02/house.jpg" alt="Girl and Mother conversation" title="Girl and Mother conversation" width="100%" height="auto">
	<div class="hero-text">
		
		<div class="loan-officer-meta">
			<h2 class="loan-officer-name"><?php the_title(); ?></h2>
			<?php
				echo '<p>'.get_post_meta(get_the_id(), 'lo_street_address', true ).'</p>';
							echo '<p>'.get_post_meta(get_the_id(), 'lo_street_address_two', true ).'</p>';
							echo '<p>'.get_post_meta(get_the_id(), 'lo_city', true ).', '.get_post_meta(get_the_id(), 'lo_state', true ).' '.get_post_meta(get_the_id(), 'lo_zipcode', true ).'</p>';
							echo '<p>Phone: '.get_post_meta(get_the_id(), 'lo_phone', true ).'</p>';
			?>
		</div>
	</div>
</div>
<article>
	<?php 
		while ( have_posts() ) : the_post();
	the_post_thumbnail();
	the_content();

	?>
<div class="clear"></div>
	<?php
endwhile;
	?>
</article>
<?php get_footer(); ?>