<?php get_header(); ?>
<link rel="stylesheet" href="<?php echo plugins_url('mortgage-solutions-loan-officers/css/mslo.css');?>">
<div class="hero-box loan-officers" >
	<img style="opacity: 1;" src="https://msdev.shomptonlabs.com/wp-content/uploads/2018/02/house.jpg" alt="Girl and Mother conversation" title="Girl and Mother conversation" width="100%" height="auto">
	<div class="hero-text">
		<?php the_post_thumbnail(); ?>
		<div class="loan-officer-meta">
			<h2 class="loan-officer-name"><?php echo get_post_meta( get_the_ID(), 'lo_first_name', true );?> <?php echo get_post_meta( get_the_ID(), 'lo_last_name', true );?></h2>
			<p><?php echo get_post_meta( get_the_ID(), 'lo_title', true );?></p>
			<p>NMLS <?php echo get_post_meta( get_the_ID(), 'lo_nmls', true );?>, MLO <?php echo get_post_meta( get_the_ID(), 'lo_mlo', true );?></p>
			<p><?php echo get_post_meta( get_the_ID(), 'lo_phone', true );?></p>
		</div>
	</div>
</div>
<article>
	<?php 
		while ( have_posts() ) : the_post();
	
	the_content();
	$surefire = '';
	if (get_post_meta( get_the_ID(), 'lo_surefire', true ) != '') {
		$surefire .= get_post_meta( get_the_ID(), 'lo_surefire', true );
	} else {
		$surefire .= 'OMa9Uv';
	}
	?>
	
<div class="tomn-form-embed" data-form-id="vN5Hg" data-user-id="<?php echo $surefire; ?>"></div><script async src="//sf3.tomnx.com/formembed/embedform.js"></script>
<div class="clear"></div>
	<?php
endwhile;
	?>
</article>
<?php get_footer(); ?>